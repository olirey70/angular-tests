import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child2',
  template: `
    <div style="background-color: cyan; padding: 10px">
        <h1>Voici module Child2</h1>
        <h2>
            <a  routerLink="page-c"
                routerLinkActive="active">Affiche page-c ci-dessous</a>
            <br>
            <a  routerLink="page-d"
                routerLinkActive="active">Affiche page-d ci-dessous</a>
        </h2>
        <router-outlet></router-outlet>
    </div>
  `
})
export class Child2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}