import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Child2Component } from './child2.component';
import { PageCComponent } from './page-c/page-c.component';
import { PageDComponent } from './page-d/page-d.component';

const child2Routes: Routes = [
    /*{
        path: '',
        redirectTo: '/page-a',
        pathMatch: 'full'
    },*/
    {
        path: 'child2',
        component: Child2Component,
        children: [
            {
                path: 'page-c',
                component: PageCComponent/*,
                outlet: 'child1Outlet'*/
            },
            {
                path: 'page-d',
                component: PageDComponent/*,
                outlet: 'child1Outlet'*/
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(child2Routes)
    ],
    exports: [RouterModule],
})
export class Child2RoutingModule {}
