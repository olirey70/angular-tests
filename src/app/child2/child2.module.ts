import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Child2Component } from './child2.component';
import { PageCComponent } from './page-c/page-c.component';
import { PageDComponent } from './page-d/page-d.component';

import { Child2RoutingModule } from './child2.routes';

@NgModule({
  imports: [
    CommonModule,
    Child2RoutingModule
  ],
  declarations: [
    Child2Component,
    PageCComponent,
    PageDComponent
  ]
})
export class Child2Module { }
