import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const appRoutes: Routes = [
    {
        path: 'child1',
        loadChildren: 'app/child1/child1.module#Child1Module'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
                preloadingStrategy: PreloadAllModules
            }
        )
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
