import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';

import { Child2Module } from './child2/child2.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Child2Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
