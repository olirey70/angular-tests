import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child1',
  template: `
    <div style="background-color: grey; padding: 10px">
        <h1>Voici module Child1</h1>
        <h2>
            <!--a  routerLink="[{ outlets: { child1Outlet: ['page-a'] } }]"
                routerLinkActive="active">Affiche page-a ci-dessous, dans son propre router-outlet</a>
            <br>
            <a  routerLink="[{ outlets: { child1Outlet: ['page-b'] } }]"
                routerLinkActive="active">Affiche page-b ci-dessous</a-->

            <a  routerLink="page-a"
                routerLinkActive="active">Affiche page-a ci-dessous</a>
            <br>
            <a  routerLink="page-b"
                routerLinkActive="active">Affiche page-b ci-dessous</a>
        </h2>
        <!--router-outlet name="child1Outlet"></router-outlet-->
        <router-outlet></router-outlet>
    </div>
  `
})
export class Child1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}