import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Child1Component } from './child1.component';
import { PageAComponent } from './page-a/page-a.component';
import { PageBComponent } from './page-b/page-b.component';

import { Child1RoutingModule } from './child1.routes';

@NgModule({
  imports: [
    CommonModule,
    Child1RoutingModule
  ],
  declarations: [
    Child1Component,
    PageAComponent,
    PageBComponent
  ]
})
export class Child1Module { }
