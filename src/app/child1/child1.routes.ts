import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Child1Component } from './child1.component';
import { PageAComponent } from './page-a/page-a.component';
import { PageBComponent } from './page-b/page-b.component';

const child1Routes: Routes = [
    /*{
        path: '',
        redirectTo: '/page-a',
        pathMatch: 'full'
    },*/
    {
        path: '',
        component: Child1Component,
        children: [
            {
                path: 'page-a',
                component: PageAComponent/*,
                outlet: 'child1Outlet'*/
            },
            {
                path: 'page-b',
                component: PageBComponent/*,
                outlet: 'child1Outlet'*/
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(child1Routes)
    ],
    exports: [RouterModule],
})
export class Child1RoutingModule {}
